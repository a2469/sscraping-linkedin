from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
from random import choice
import pandas as pd
import pickle
import time
import re
import os

PROXIES = pd.read_csv('proxies.txt', encoding='utf-8-sig', delimiter="\n").to_numpy()
indexP = 0

def GetProxy():
    global indexP

    proxy = PROXIES[indexP][0]
    ip = proxy.split(":")[0]
    port = proxy.split(":")[1]
    user = proxy.split(":")[2]
    password = proxy.split(":")[3]
    proxy = {'http': 'http://' + user + ':' + password + '@' + ip + ':' + port}
    indexP += 1
    if indexP == 50:
        indexP = 0
    return proxy

class LinkedIn():
    def __init__(self, email, password, title, message, browser='Chrome'):
        self.LOGIN_URL = 'https://www.linkedin.com/login'

        self.email = email
        self.password = password
        self.title = title
        self.message = message
        
    def createDriver(self):
        self.proxies = GetProxy()

        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)
        profile = webdriver.FirefoxProfile()
        options = webdriver.FirefoxOptions()
        options.set_preference("dom.webnotifications.serviceworker.enabled", False)
        options.set_preference("dom.webnotifications.enabled", False)
        options.add_argument('--proxy-server=http://%s' % self.proxies)
        self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(), options=options)
        return self.driver

    def Login(self):
        self.driver = self.createDriver()
        self.driver.get(self.LOGIN_URL)
        time.sleep(1)
        email_element = self.driver.find_element_by_id('username')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_id('password')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_xpath("//div[@class='login__form_action_container ']")
        login_button.click()
        for i in range(1,20):
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(2)

if __name__=="__main__":
    linkedin = LinkedIn(email='dhhd@gmail.com', password='hdhdh', title='data scientist',message='Bonjour Mr.', browser='Chrome')
    linkedin.Login()

